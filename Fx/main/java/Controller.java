import dao.SongDao;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Artist;
import model.Genre;

import java.util.*;

public class Controller {

    private ObservableList<String> genge = FXCollections.observableArrayList();
    @FXML
    private ComboBox<String> comboGenre = new ComboBox<>();

    ObservableList<String> artist = FXCollections.observableArrayList();
    @FXML
    private ComboBox<String> comboArtist = new ComboBox<>();

    @FXML
    private TableView<SongTable> table;

    @FXML
    private TableColumn<SongTable,String> columSong;

    @FXML
    private TableColumn<SongTable,String> columArtist;

    @FXML
    private TableColumn<SongTable,String> columGenge;

    private ObservableList<SongTable> songTableData;
    private List songs= new ArrayList();

    public void initialize() {
        columSong.setCellValueFactory(new PropertyValueFactory<SongTable, String>("name"));
        columArtist.setCellValueFactory(new PropertyValueFactory<SongTable, String>("artist"));
        columGenge.setCellValueFactory(new PropertyValueFactory<SongTable, String>("genre"));
        Genre genre = (Genre) SongDao.getGenre().get(0);
        genge.addAll(genre.getGenre());
        comboGenre.setItems(genge);

    }

    @FXML
    private void open(){
        songs = SongDao.getSongByArtist(comboArtist.getValue());
        songTableData = FXCollections.observableArrayList();
        songTableData.addAll(songs);
        table.setItems(songTableData);
    }

    @FXML
    private void actionComboGenre(){
        artist.clear();

        List<Artist>  artists = SongDao.getArtistsByGenre(comboGenre.getValue());
        for (Artist artist:artists) {
            this.artist.add(artist.getArtists());
        }

        artist.addAll();
        comboArtist.setItems(artist);
    }



}
