package dao;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import model.Artist;
import model.Genre;
import model.Song;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.util.ArrayList;
import java.util.List;

public class SongDao {
    public static void main(String[] args) {

    }
    public static List getGenre() {
        List<Genre> genres = new ArrayList<>();
        try {
            String url = "http://localhost:8080/sample_servlet_war_exploded/getGenre";
            Document doc = Jsoup.connect(url).ignoreContentType(true).get();
            System.out.println(doc.text());
            genres = new Gson().fromJson(String.valueOf(doc.text()),
                    new TypeToken<List<Genre>>(){}.getType());

        } catch (Exception ex) {
            System.err.println(ex);

        }
        return genres;
    }
    public static List getArtistsByGenre(String genre) {
        List<Artist> artists = new ArrayList<>();
        try {
            String url = "http://localhost:8080/sample_servlet_war_exploded/getArtists?genre="+genre;
            System.out.println(url);
            Document doc = Jsoup.connect(url).ignoreContentType(true).get();
            System.out.println(doc.text());

            artists = new Gson().fromJson(String.valueOf(doc.text()),
                    new TypeToken<List<Artist>>(){}.getType());

        } catch (Exception ex) {
            System.err.println(ex);
        }
            return artists;
    }
    public static List getSongByArtist(String genre) {
        List<Song> songs= new ArrayList<>();
        try {
            String url = "http://localhost:8080/sample_servlet_war_exploded/getSong?artist="+genre;
            Document doc = Jsoup.connect(url).ignoreContentType(true).get();
            System.out.println(doc.text());
            songs = new Gson().fromJson(String.valueOf(doc.text()),
                    new TypeToken<List<Song>>(){}.getType());
        } catch (Exception ex) {
            System.err.println(ex);
        }
        return songs;
    }
}
