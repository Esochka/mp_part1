package model;

import java.util.List;

public class Genre {

    private List<String> genre;


    public Genre(List<String> genre) {
        this.genre = genre;
    }

    public List<String> getGenre() {
        return genre;
    }

    public void setGenre(List<String> genre) {
        this.genre = genre;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "genre=" + genre +
                '}';
    }
}
